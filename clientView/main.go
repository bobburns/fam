package main

import (
	"flag"
	"fmt"
	session "gitlab.com/bobburns/famsess"
	//"io/ioutil"
	"crypto/sha256"
	"golang.org/x/crypto/ssh/terminal"
	"net"
	"os"
	"syscall"
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {

		fmt.Println("Usage: %s <host:port>", os.Args[0])
		os.Exit(1)
	}
	serv := flag.Arg(0)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serv)
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	sess, err := Login(conn)
	if err != nil {
		fmt.Println("Sorry bad login.")
		os.Exit(1)
	}
	fmt.Println("Authorize Successfull")
	//os.Exit(0)
	run(sess)
}

func Login(conn *net.TCPConn) (*session.Session, error) {

	s := session.NewSession(conn)
	fmt.Print("Please enter username: ")
	user := ""
	fmt.Scan(&user)
	fmt.Print("Please enter password: ")
	// from stackoverflow http://stackoverflow.com/questions/2137357/getpasswd-functionality-in-go
	bpass, err := terminal.ReadPassword(int(syscall.Stdin))
	checkError(err)

	fmt.Println()
	fmt.Println("Authorizing...")

	spass := sha256.Sum256(bpass)
	pass := fmt.Sprintf("%x", spass)
	fmt.Println(pass)
	err = s.ClientAuth(user, pass)
	checkError(err)
	//s.SendAck()
	return s, nil
}

func checkError(err error) {
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}
