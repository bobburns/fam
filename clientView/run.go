package main

import (
	"fmt"
	g "github.com/tncardoso/gocurses"
	disp "gitlab.com/bobburns/famdisp"
	session "gitlab.com/bobburns/famsess"
	"os"
)

func run(fams *session.Session) {
	fmt.Println("Loading Messages...")

	ms, err := loadNewMessages(fams)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	totalm := len(ms)
	index := totalm - 1

	//start window
	w := g.Initscr()
	g.Noecho()
	defer g.End()
	//test := fmt.Sprintf("mail %s\n", fams.Mail.Files[0])
	//g.Addstr(test)
	g.Addstr("Press any key to continue")

	row, _ := g.Getmaxyx()
	h := row - 6
	winMain := g.NewWindow(h, 50, 0, 0)
	winText := g.NewWindow(5, 80, h, 0)
	winText.Border(' ', ' ', '+', ' ', '+', '+', ' ', ' ')
	sets := int(h / 6)
	md := &disp.MessDisplay{
		Window:   winMain,
		Index:    index,
		Set:      sets,
		Messages: ms,
		Rows:     row,
	}
	g.Getch()
	helpmenu()

	md.ShowMessages()
	winText.Refresh()
	w.Refresh()
	for {
		inchar := w.Getch()
		switch inchar {
		case 'e':
			g.End()
			os.Exit(0)
		case 'n':
			md.Index -= md.Set
			if md.Index < 0 {
				md.Index = 0
			}
			md.ShowMessages()
			str := fmt.Sprintf("%d / %d", md.Index, len(md.Messages))
			md.DisplayStr(str)
		case 'b':
			md.Index += md.Set
			if md.Index > totalm-1 {
				md.Index = totalm - 1
			}
			md.ShowMessages()
			str := fmt.Sprintf("%d / %d", md.Index, len(md.Messages))
			md.DisplayStr(str)
		case 'g':
			md.Index = totalm - 1
			md.ShowMessages()
			str := fmt.Sprintf("%d / %d", md.Index, len(md.Messages))
			md.DisplayStr(str)
		case 'G':
			md.Index = 0
			md.ShowMessages()
			str := fmt.Sprintf("%d / %d", md.Index, len(md.Messages))
			md.DisplayStr(str)
		case '@':
			// clear message
			md.DisplayStr("Insert")
			md.GetText(winText, true)
			md.DisplayStr("")
		case '^':
			md.DisplayStr("Insert")
			if len(md.Out.Body) > 0 {
				md.GetText(winText, false)
			} else {
				md.GetText(winText, true)
			}
			md.DisplayStr("")
		case 's':
			md.DisplayStr("Sending Text...")
			err := md.Send(fams)
			if err != nil {
				if err.Error() == "Not Fam. No Keys" {
					md.DisplayStr(err.Error())
					continue
				}
				cleanup(err)
			}
			md.DisplayStr("Message Sent!")
			//g.End()
			winText.Mvaddstr(1, 0, "\n\n\n\n")
			winText.Refresh() // clear text
			md.Out = disp.Message{}
			// TODO send message and clear md.Out
		case 'p':
			md.Window.Mvaddstr(0, 0, string(md.Out.Body))
			md.Window.Refresh()
		case 'L':

			md.DisplayStr("Loading Messages...")
			new, err := loadNewMessages(fams)
			if err != nil {
				cleanup(err)
			}
			md.Messages = new
			totalm = len(new)
			md.Index = totalm - 1
			md.DisplayStr("")
			md.ShowMessages()
			str := fmt.Sprintf("%d / %d", md.Index, len(md.Messages))
			md.DisplayStr(str)
		case 'D':
			md.DisplayStr("Enter message number to delete + M")
			err := md.Delete(fams)
			if err != nil {
				cleanup(err)
			}
			md.DisplayStr("Delete Success")
		case 'C':
			err := md.ChangePassword(fams)
			if err != nil {
				cleanup(err)
			}
			md.DisplayStr("Change Password Success")

		}
		//g.Refresh()
	}
	g.End()
}

func helpmenu() {

	winHelp := g.NewWindow(15, 20, 1, 51)
	winHelp.Box(0, 0)
	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(1, 1, 'n')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("next")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(3, 1, 'b')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("back")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(5, 1, 'e')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("end")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(7, 1, 'g')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("first")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(9, 1, 'G')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("last")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(1, 9, '^')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("toggle")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(3, 9, '@')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("new text")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(5, 9, 's')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("send text")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(7, 9, 'L')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("load msg")

	winHelp.Attron(g.A_REVERSE)
	winHelp.Mvaddch(9, 9, 'C')
	winHelp.Attroff(g.A_REVERSE)
	winHelp.Addstr("chg pass")
	winHelp.Refresh()

}
func loadNewMessages(fams *session.Session) ([]disp.Message, error) {

	err := fams.RequestInitMess()
	if err != nil {
		cleanup(err)
	}
	err = fams.RequestMeta()
	if err != nil {
		cleanup(err)
	}
	ms := []disp.Message{}
	for _, file := range fams.Mail.Files {
		//skip initial message
		if file == "m0" {
			continue
		}
		//	fmt.Println("mail: ", file)
		err := fams.MessageGet(file)
		if err != nil {
			fmt.Println(err)
			return ms, err
		}
		d, err := fams.DecryptMessage()
		if err != nil {
			d = "From:??\nDate:0\nThis message could not be decrypted.\nPlease Delete."
		}
		mess, err := disp.ParseMessage(d)
		if err != nil {
			fmt.Println(err)
			return ms, err
		}
		mess.File = file
		/*
			mess := disp.Message{
				From: "bla",
				Date: time.Now().Unix(),
				Num:  file,
				Body: []byte(d),
			}
		*/
		ms = append(ms, mess)

	}
	return ms, nil
}

func cleanup(err error) {
	g.End()
	fmt.Println("uh oh, something went wrong")
	fmt.Println(err)
	os.Exit(1)
}
