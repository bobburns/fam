#!/bin/bash

# set up client in clientView dir
# admin needs to transfer keypub.pem to server users/fam/
# and transerver server_pub.pem to clientView

if [ -z "$1" ]
	then
		echo "usage $0 client"
		exit 1
fi

mkdir -p $1/inbox
echo "{\"last\":0,\"path\":\"$1/inbox/\",\"file\":[\"m0\"]}" > $1/inbox/metafile.json
touch $1/inbox/m0

mkdir -p $1/outbox
echo "{\"last\":0,\"path\":\"$1/outbox/\",\"file\":[\"m0\"]}" > $1/outbox/metafile.json
touch $1/outbox/m0
# setup keys

openssl genrsa -out $1/keypriv.pem 2048
openssl rsa -in $1/keypriv.pem -pubout > $1/keypub.pem

