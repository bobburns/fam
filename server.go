package main

import (
	"bytes"
	"fmt"
	"gitlab.com/bobburns/famsess"
	"log"
	"net"
	"os"
)

func main() {
	service := ":16538"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		defer conn.Close()
		fmt.Println("listening on port ", service)
		go handleClient(conn)
	}
}
func handleClient(conn net.Conn) {
	s := session.NewSession(conn)
	log.Printf("Got connection from: %s\n", conn.RemoteAddr().String())

	for {
		err := s.ParseIncoming()
		if err != nil {
			log.Printf("Error ParseIncoming [%s]\n", err)
			break
		}

		log.Printf("incomming op: %x\n", s.RecOp[0:4])
		switch {
		//		case bytes.Equal(s.RecOp[0:4], session.KeyRequest[0:4]):
		//			log.Println("got KeyRequest")
		//			err = s.SendKey()
		case bytes.Equal(s.RecOp[0:4], session.SendMPre[0:4]):
			err = s.MessagePreRecv()
		case bytes.Equal(s.RecOp[0:4], session.SendMess[0:4]):
			err = s.MessageRecv()
		case bytes.Equal(s.RecOp[0:4], session.RetrMess[0:4]):
			err = s.MessageRetr()
		case bytes.Equal(s.RecOp[0:4], session.ClientAuth[0:4]):
			err = s.ServerAuth()
		case bytes.Equal(s.RecOp[0:4], session.MessInit[0:4]):
			err = s.MessageInit()
		case bytes.Equal(s.RecOp[0:4], session.ReqMeta[0:4]):
			err = s.ServMeta()
		case bytes.Equal(s.RecOp[0:4], session.DelMess[0:4]):
			err = s.MessageServDel()
		case bytes.Equal(s.RecOp[0:4], session.CChPass[0:4]):
			err = s.ServerChgPass()

		}
		if err != nil {
			// how to handle errors?
			log.Println(err)
		}
	}
	log.Printf("Lost connection from: %s\n", conn.RemoteAddr().String())
	conn.Close()
}

func checkError(err error) {
	if err != nil {
		log.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}
