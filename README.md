***FAM***

**A private messaging app for pocketCHIP**

This is an experimental text based messaging app to use with the pocketCHIP.

It requires at least 1 pocketCHIP and a CHIP or Raspberry PI for the server.


What I'm trying to do is create a messaging app where the messages are encrypted in a way
that only the client can read them.  The server stores the encrypted data but cannot decrypt it. Symmetric aes keys are used between clients to message each other, and RSA session keys are used to communicate with the server.

To accomplish this there may be too many keys/passwords to be easy to use.  But hey I'm just having fun.

Each client needs a pair of keys (ascii text) and a password.  RSA keys are generated during setup for the client and server.

**Installation**

Setup Server [wiki](https://gitlab.com/bobburns/fam/wikis/how-to-setup-fam-server)

Setup Client [wiki](https://gitlab.com/bobburns/fam/wikis/set-up-client)

**Important**

A copy of server\_pub.key goes in clientView/(user)/server\_pub.pem on Client pocketCHIP

A copy of (user) keypub.pem goes into fam/users/(user)/keypub.pem on Server CHIP


Start server `./server`  note this is setup to use port 16538

Start client on pocketCHIP `./clientView <ip to local server>:16538`

Start making super secret plans

**Usage**

Basic key commands [wiki](https://gitlab.com/bobburns/fam/wikis/basic-usage)

